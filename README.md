# Add Days #

Interview exercise with Cloudbees.com 

### Shows a possible solution with tests ###

* For a custom type class XDate, implement addDays function.

### How to run ###

* Dependencies: Needs Maven 3+ and repo acces for dependencies

### Steps: ###
* git clone
* mvn test
