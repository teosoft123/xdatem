package com.teosoft.date;


import org.testng.AssertJUnit;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class XDateTest {

	@Test(dataProvider = "positiveTests")
	public void testXDate(int days, XDate initial, XDate expected) {
		AssertJUnit.assertEquals(initial.addDays(days), expected);
	}

	@Test(dataProvider = "negativeTests", expectedExceptions = {IllegalArgumentException.class})
	public void testXDateNegative(int days, XDate initial, XDate expected) {
		AssertJUnit.assertEquals(initial.addDays(days), expected);
	}
	
	@DataProvider
	public Object[][] positiveTests() {
		return new Object[][] { new Object[] { 0, new XDate(0, 0, 0), new XDate(0, 0, 0) },
				new Object[] { 1, new XDate(0, 0, 0), new XDate(0, 0, 1) },
				new Object[] { 32, new XDate(0, 0, 0), new XDate(0, 1, 1) },
				new Object[] { 31 + 28, new XDate(0, 0, 0), new XDate(0, 2, 0) },
				new Object[] { 31 + 27, new XDate(0, 0, 0), new XDate(0, 1, 27) },
				new Object[] { 365, new XDate(0, 0, 0), new XDate(1, 0, 0) },
				new Object[] { 366, new XDate(0, 0, 0), new XDate(1, 0, 1) },
				new Object[] { 365, new XDate(1, 0, 0), new XDate(2, 0, 0) },
		};
	}

	@DataProvider
	public Object[][] negativeTests() {
		return new Object[][] {
				 new Object[] {-1, new XDate(0,1,0), new XDate(0,0,31)}
		};

	}
	
}
